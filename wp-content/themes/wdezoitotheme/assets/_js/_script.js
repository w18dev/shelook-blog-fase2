$(document).ready(function(){
	var carousel = $("#banner--carousel");
	carousel.owlCarousel({
		items:1,
		autoplay: true, 
		autoplayTimeout: 4000,
		loop: true,
		animateIn: "fadeIn",
		animateOut: "fadeOut",
		mouseDrag: false
	});
	var carouselLoja = $("#loja--carousel");
	carouselLoja.owlCarousel({
		items:1,
		autoplay: true, 
		autoplayTimeout: 4000,
		loop: true,
		smartSpeed: 1000,
		responsive:{
			320:{
				items:1,
			}
		}
	});
	var carouselSlide = $("#slider--carousel");
	carouselSlide.owlCarousel({
		items:4,
		autoplay: true,
		autoplayTimeout: 4000,
		smartSpeed: 1000,
		loop: true,
		responsive:{
			320:{
				items:1,
			},
			485:{
				items:2,
			},
			940:{
				items:3,
			},
			1024:{
				items:4
			}
		}
	});

	var carouselCategory = $("#carousel--category");
	carouselCategory.owlCarousel({
		items:8,
		responsive:{
			320:{
				items:3,
			},
			375:{
				items:4,
			},
			768:{
				items:6,
			},
			1024:{
				items:6
			}
		}
	});

							
	var carouselRelated = $("#related--carousel");
	carouselRelated.owlCarousel({
		items:2,
		loop: true,
		responsive:{
			320:{
				items:1,
			},
			560:{
				items:2,
			},
			1024:{
				items:2
			}
		}
	});
	var carouselDicas = $(".dicas--carousel");
	carouselDicas.owlCarousel({
		items:5,
		autoplay: true,
		autoplayTimeout: 4000,
		smartSpeed: 1000,
		loop: true,
		responsive:{
			320:{
				items:2,
			},
			480:{
				items:3,
			},
			600:{
				items:4,
			},
			732:{
				items:5
			}
		}
	});
	$('#button-next').click(function(){
		carouselLoja.trigger('next.owl.carousel');
	});
	$('#button-prev').click(function(){
		carouselLoja.trigger('prev.owl.carousel');
	});
	$('#button-next--slider').click(function(){
		carouselSlide.trigger('next.owl.carousel');
	});
	$('#button-prev--slider').click(function(){
		carouselSlide.trigger('prev.owl.carousel');
	});
	$('.button-next--dicas').click(function(){
		carouselDicas.trigger('next.owl.carousel');
	});
	$('.button-prev--dicas').click(function(){
		carouselDicas.trigger('prev.owl.carousel');
	});
	$('#button-next--related').click(function(){
		carouselRelated.trigger('next.owl.carousel');
	});
	$('#button-prev--related').click(function(){
		carouselRelated.trigger('prev.owl.carousel');
	});
	$('#button-next--menu').click(function(){
		carouselCategory.trigger('next.owl.carousel');
	});
	$('#button-prev--menu').click(function(){
		carouselCategory.trigger('prev.owl.carousel');
	});
	var cidadeestado = new CidadeEstado();
	cidadeestado.listarEstados({
		selector: "ddlEstado"
	});

	 $("#ddlEstado").on("change", function(){
	 	/*selectClear("ddlCidade");*/
	 	var idEstado = $(this).val();
	 	cidadeestado.listarCidades({
	 		selector: "ddlCidade", 
	 		estado: idEstado
	 	})
	 });

	 $("#Enviar").on("click", function(e){
	 	e.preventDefault();
	 	if(formCheckRequerid()){
	 		$("#Contato").submit();
	 	}
	 })

	$("#search").click(function(){
		if($("#pesquisa").hasClass("open")){
			$("#pesquisa").removeClass("open");
		}
		else{
			$("#pesquisa").addClass("open");
		}
	});
	$("#search-mobile").click(function(){
		if($("#pesquisa-mobile").hasClass("open")){
			$("#pesquisa-mobile").removeClass("open");
		}
		else{
			$("#pesquisa-mobile").addClass("open");
		}
	});

	/*
		################
		  MENU SIDEBAR
		################
	*/
	$(document).ready(function(){
		$('#open-menu').click(function(){
			if($(this).hasClass('mov')){
				$(this).removeClass('mov');
				$('#close-menu').css({'left' : '-100%'});
			}else{
				$(this).addClass('mov');
				$('#close-menu').css({'left' : '0'});
			}
		});

	});
	/*
		################
		 //MENU SIDEBAR
		################
	*/

	/*
		####################
		  SUBMIT COM ENTER
		####################
	*/

		$('#Pesquisa-index input').keydown(function(e) {
		    if (e.keyCode == 13) {
		        $('#Pesquisa-index').submit();
		    }
		});
		$('#Pesquisa-category input').keydown(function(e) {
		    if (e.keyCode == 13) {
		        $('#Pesquisa-category').submit();
		    }
		});
		$('#Pesquisa-post input').keydown(function(e) {
		    if (e.keyCode == 13) {
		        $('#Pesquisa-post').submit();
		    }
		});
		$('#Pesquisa-header input').keydown(function(e) {
		    if (e.keyCode == 13) {
		        $('#Pesquisa-header').submit();
		    }
		});
		$('#Pesquisa-header--mobile input').keydown(function(e) {
		    if (e.keyCode == 13) {
		        $('#Pesquisa-header--mobile').submit();
		    }
		});
		
		$('#Pesquisa-search input').keydown(function(e) {
		    if (e.keyCode == 13) {
		        $('#Pesquisa-search').submit();
		    }
		});

	/*
		####################
		  //SUBMIT COM ENTER
		####################
	*/

		$('#topo').click(function (){

			$('body,html').animate({

				scrollTop: 0

			}, 'slow');

			return false;

		});

		$(window).on("scroll", function(){
			if(getDevice() == 'mobile' || getDevice() == 'tablet'){
				if($('.icon-menu').hasClass('mov')){
					$('.icon-menu').removeClass('mov');
					$('#close-menu').css({'left' : '-100%'});
				}
			}

		});



});

