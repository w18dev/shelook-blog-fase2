$(document).ready(function(){

	var window_w = $(window).width();

	if(window_w < 992){
		menu_responsive();
	}else{
		menu_responsive_destroy();
	}

	$(window).resize(function(){
		window_w = $(window).width();
		if(window_w < 992){
			menu_responsive();
		}else{
			menu_responsive_destroy();
		}
	});
});
function menu_responsive(){
	var button 	= document.createElement('button'),
		bar    	= document.createElement('s');

	button.setAttribute('class', 'menu-icon');
	button.setAttribute('id', 'menu-open');
	bar.setAttribute('class', 'bar');
	button.appendChild(bar);

	if(!$('.menu-responsive').hasClass('mobile')){
		$('.menu-responsive').addClass('mobile');
		$('.menu-responsive').append(button);

		$('.menu-list li').each(function(){
			console.log(this.val);
		});
	}
}

function menu_responsive_destroy(){
	var button = document.getElementById('menu-open');

	if($('.menu-responsive').hasClass('mobile')){
		$('.menu-responsive').removeClass('mobile');
		button.remove(button);
	}
}