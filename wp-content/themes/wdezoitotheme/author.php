<?php include 'header.php'; ?>
          <div class="row container center col-lg-12 index--align">
               <div class="col-lg-12 row">
                   <div class="posts-block col-lg-8 left">
                       <div class="posts-block--title">
                          <h1>Arquivo de Autor</h1>
                       </div>
                       <?php 
                                   $args = array(
                                      'author' => get_post_field( 'post_author', $post_id),
                                      'order' => 'ASC',
                                      'posts_per_page' => -1,
                                    );  
                                   query_posts($args);
                                   if (have_posts()): while (have_posts()) : the_post();

                                      //$imagem = get_post_meta($postId, 'wpcf-imagem-do-post', true);
                                      $imagem = get_field('imagem_do_post', $post_id);
                                      $conteudo = removeParagraph(get_field('conteudo_do_post', $post_id));

                                      //if (function_exists('wp_corenavi')) {wp_reset_query();}                  
                         ?>
                        <div class="row posts-container"> 
                            <a name="POSTS"></a>
                            <div class="posts-img col-lg-5">
                                <a href="<?php the_permalink(); ?>"><img src="<?php echo $imagem; ?>" /></a>   
                            </div>
                            <div class="posts-content container col-lg-6">
                                <div class="posts-category">
                                    <span><?php the_category(); ?></span>
                                </div>
                                <div class="posts-title">
                                   <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                                </div>
                                <div class="posts-author">
                                    <span><?php the_modified_time('d/m/Y'); ?> por <i><?php the_author(); ?></i></span>
                                </div>
                                <div class="posts-about">
                                    <p><?php echo $conteudo ?></p>
                                </div>
                                <div class="posts-ver">
                                    <a href="<?php the_permalink(); ?>">VER O POST</a>
                                </div>
                                <div class="posts-social">
                                    <div class="posts-social--facebook text-center">
                                        <a href="#"><i class="fa fa-facebook"></i> COMPARTILHE </a>   
                                    </div>
                                    <div class="posts-social--twitter text-center">
                                        <a href="#"><i class="fa fa-twitter"></i> TWEET </a>
                                    </div>
                                    <div class="posts-social--pinit text-center">
                                        <a href="#"><i class="fa fa-pinterest-p"></i> PIN IT </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; endif;
                            wp_reset_query();
                        ?>
                        <div class="row container text-center pagenavi col-lg-11">
                            <div id="pagination">
                                <div class="nav-pag">
                                    <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
                                </div>
                                <div class="navigation">
                                    <div class="nav-previous--author"><?php previous_posts_link( __( '< ', 'shelook-blog-fase2' ) ); ?></div>
                                    <div class="nav-next--author"><?php next_posts_link ( __( ' >', 'shelook-blog-fase2' ) ); ?></div>
                                </div>  
                            </div>
                        </div>
                   </div>
                   <div class="right-block col-lg-4 right">
                      <div class="container sobre-block">
                          <div class="container">
                              <!--INFORMAÇÕES DA PAGINA SOBRE NÓS--> 
                              <?php
                                $post = get_post('166'); 
                                $postId = $post->ID;
                                $imagem_sobre =  get_field('imagem_rodape');
                                $subt_sobre =  get_field('subtitulo');
                                $conteudo =  get_field('conteudo_reduzido');
                                $imagem_pagina =  get_field('imagem_da_pagina');

                              ?> 
                              <!--//INFORMAÇÕES DA PAGINA SOBRE NÓS-->                                 
                              <div class="sobre-title">
                                 <p>SOBRE NÓS</p>
                              </div>
                              <div class="sobre-img">
                                  <img src="<?php echo $imagem_pagina ?>" /> 
                              </div>
                              <div class="sobre-text">
                                  <?php echo $conteudo ?>
                                  <div class="sobre-logo right">
                                      <a href="#"><img src="<?php echo $imagem_sobre ?>"></a>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="categoria-block">
                          <div class="categoria-search">
                              <div class="categoria-icon">
                                  <img src="<?php bloginfo('template_directory') ?>/images/icon-pesquisa.png" />
                              </div>
                              <form id="Pesquisa-category" action="<?php get_home_url(); ?>">
                                  <input type="text" name="s" placeholder="Pesquisa"/>
                              </form>
                          </div>
                          <div class="categoria-list">
                              <h2>CATEGORIAS</h2>
                              <nav>
                                  <ul>
                                      <?php
                                            $categories=get_categories();


                                            foreach($categories as $category) {
                                                echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens em %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                                            }
                                        ?>
                                  </ul>
                              </nav>  
                          </div>
                      </div>
                      <div class="posts--block">
                          
                      </div>
                      <div class="social-right--facebook col-lg-12">
                          <div class="fb-page" data-href="https://www.facebook.com/shelook.semijoias/" data-tabs="timeline" data-width="369" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/shelook.semijoias/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/shelook.semijoias/">Shelook Semijoias</a></blockquote></div>
                      </div>
                      <div class="social-right--instagram">
                          <!-- SnapWidget -->
                          <div class="insta-container">
                              <div class="row">
                                  <div class="inta-img container col-lg-3 left">
                                      <a href="https://www.instagram.com/shelook.semijoias/"><img src="<?php bloginfo('template_directory') ?>/images/instagram.jpg"></a>
                                  </div>
                                  <div class="insta-text container col-lg-9 left">
                                      <h1>INSTAGRAM</h1>
                                      <h2><a href="https://www.instagram.com/shelook.semijoias/">@shelook.semijoias</a></h2>
                                  </div>
                                  <!-- SnapWidget -->
                                  <!-- SnapWidget -->
                                  <script src="https://snapwidget.com/js/snapwidget.js"></script>
                                  <iframe src="https://snapwidget.com/embed/code/237602" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
                              </div>
                          </div>
                      </div>
                   </div>
               </div>
          </div>

<?php include 'footer.php'; ?>
<script type="text/javascript">
  $(window).load(function(){
    var category  = <?=$aux?>,
        url       = <?php bloginfo('url'); ?>
    selecionarItemMenu(url, category);
    $("a[href='http:uahsaushuahsuas/asuhasuhas/category/nome']").addClass("active");
  });
  function selecionarItmeMenu(url, category){
    $("a[href="+url+"/category/"+category+"']").addClass("active");
  }
</script>

            