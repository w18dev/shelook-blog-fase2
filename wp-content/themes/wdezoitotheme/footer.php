	<footer class="footer text-center">
		<div class="footer-title">
			<p>NOS SIGA NO INSTAGRAM</p>
		</div>
		<div class="footer-slider--instagram">
			<!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/2a13c45abfa052ac83f57ec2c75ea8a6.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
		</div>
		<div class="container center">
			<div class="footer-icon left">
				<a href="#"><img src="<?php bloginfo('template_directory') ?>/images/logo-footer.png"></a>
			</div>
			<div class="footer-copiryght right">
				<div class="lato-regular col-xs-12">
					<p>© <?php echo date('Y'); ?> SHELOOK SEMI JOIAS. Todos os direitos reservados</p>
				</div>
				<div class="triangulo pracima oswald-bold mobile-hide tablet-hide">
	                <a href="#topo">TOPO</a>
	            </div>
			</div>	
		</div>
			
	</footer>

	
	 <script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.3.min.js"></script>
	 <script src="<?php bloginfo('template_directory'); ?>/js/champs.min.js"></script>
	 <script>(function(d, s, id) {
	      var js, fjs = d.getElementsByTagName(s)[0];
	      if (d.getElementById(id)) return;
	      js = d.createElement(s); js.id = id;
	      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8";
	      fjs.parentNode.insertBefore(js, fjs);
	    }(document, 'script', 'facebook-jssdk'));
	 </script>
</body>
</html>
