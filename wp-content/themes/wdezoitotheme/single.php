<?php include 'header.php'; ?>
   <div class="container center">
      <div class="single">
         <div class="row col-lg-12 col-xs-12">
             <?php 
                    the_post();
                    $post = get_post();        
                    $postId = $post->ID;
                    // echo "<pre>";
                    // print_r($post);
                    // echo "</pre>";
                    //$imagem = get_post_meta($postId, 'wpcf-imagem-do-post', true);
                    $imagem = get_field('imagem_do_post', $postId);

                    $banner = get_field('banner_single', $postId);            
                    setPostViews(get_the_ID());

                    //Para pegar o nome da categoria 
                    $categoria      = get_the_category();
                    $nomeCategoria  = $categoria[0]->cat_name;
                    $conteudo       = get_field('conteudo_do_post', $postId);
                    $post_link      = get_permalink($post->ID);
            ?>
            
             <div class="single-posts col-lg-7 col-xs-12 left">
                  <div class="single-content">
                      <div class="single-category">
                          <span><?php the_category(); ?></span>
                      </div>
                      <div class="single-title">
                         <p><?php the_title(); ?></p>
                      </div>
                      <div class="single-author">
                          <span><?php the_modified_time('d/m/Y'); ?> por <i><a href="<?php the_permalink(); ?>"><?php the_author_nickname(); ?></a></i></span>
                      </div>
                      <div class="single-about">
                          <p><?php echo $conteudo ?></p>
                      </div>
                  </div>
                  <div class="single-img center col-xs-12">
                      <img src="<?php echo $imagem; ?>" />   
                  </div>

                  <?php 
                        
                            if(have_rows('produtos_relacionados_post')):
                              while( have_rows ('produtos_relacionados_post') ):
                                the_row();
                                $imagem_banner = get_sub_field('imagem_do_produto', $postId);  
                                $nome_do_produto = get_sub_field('nome_do_produto', $postId);  
                                $valor_do_produto = get_sub_field('valor_do_produto', $postId);  
                                $link_do_produto = get_sub_field('link_do_produto', $postId);  
                                $descricao_do_produto = get_sub_field('descricao_do_produto', $postId); 
                  ?>
                                <div class="single-category">
                                   <div class="single-category--img center">
                                      <img src="<?php echo $imagem_banner ?>" >    
                                   </div>
                                   <div class="single-category--title row">
                                      <div class="left text-align">
                                          <h1><?php echo $nome_do_produto; ?> <?php echo $valor_do_produto ?></h1>
                                      </div>
                                      <div class="left link-align">
                                          <p><a href="<?php echo $link_do_produto; ?>">COMPRAR</a></p>
                                      </div>
                                   </div>
                                   <div class="single-category--content row">
                                      <?php echo $descricao_do_produto; ?>
                                   </div>
                                </div>
                  <?php
                              endwhile;
                            endif;
                        wp_reset_query();
                  ?>

                  <!-- SINGLE CAROUSEL -->
                  <div class="dicas-block col-lg-12 row">
                      <div class="text-center dicas-title align-title--absolute">
                          <p>DICAS DO POST</p> 
                      </div>
                      <div class="dicas-block--carousel">
                          <div class="dicas-block--align">
                              <button class="button-prev--dicas"><i class="fa fa-angle-left"></i></button> 
                                  <div class="dicas--carousel">
                                  <?php 
                                      //showResult($categoria);
                                      $query_arg  = array("post_type" => "produtos", "cat" => $categoria[0]->cat_ID);
                                      $query = new WP_Query($query_arg);


                                      if($query->have_posts()):
                                        $query->the_post();
                                        while( have_rows ('produtos_relacionados') ):
                                        the_row();
                                        $post = get_post();        
                                        $postId_DP = $post->ID;
                                        $imagem = get_sub_field('imagem_do_produto', $postId_DP);  
                                        $nome = get_sub_field('nome_do_produto', $postId_DP);  
                                        $valor = get_sub_field('valor_do_produto', $postId_DP);  
                                        $link = get_sub_field('link_do_produto', $postId_DP);  

                                  ?>
                                      <div class="dicas-container item center">
                                          <div class="center dicas-container--img">
                                              <img src="<?php echo $imagem; ?>">
                                              <a href="<?php echo $link; ?>">
                                                  <div class="dicas-container--block">
                                                      <div class="dicas-container--text text-center">
                                                          <p>COMPRAR</p>
                                                      </div>
                                                  </div>
                                              </a>
                                          </div>
                                          <div class="text-center">
                                              <h1><?php echo $nome ?></h1>
                                              <h2><?php echo $valor ?></h2>
                                              <a href="<?php echo $link; ?>">COMPRAR</a>
                                          </div>
                                      </div>
                                  <?php endwhile; endif; wp_reset_query(); ?>
                                  </div>
                              <button class="button-next--dicas"><i class="fa fa-angle-right"></i></button> 
                          </div>
                      </div>
                  </div>
                  <!-- END SINGLE CAROUSEL -->
                  <div class="single-social row">
                     <div class="single-social--share left">
                        <p>COMPARTILHE</p> 
                     </div>
                     <div class="single-social--icons right">
                        <div class="mobile-hide">
                            <div class="single-social--facebook text-center">
                                <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog','width=626,height=436'); return false;"><i class="fa fa-facebook"></i> COMPARTILHE </a>   
                            </div>
                            <div class="single-social--twitter text-center">
                                <a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php echo $post_link; ?>&counturl=URL&via=USUARIO" target="_blank"><i class="fa fa-twitter"></i> TWEET </a>
                            </div>
                            <div class="single-social--pinit text-center">
                                <a href="http://pinterest.com/pin/create/button/?url=<?php echo $post_link; ?>&media=<?php echo $imagem; ?>&description=<?php echo $titulo; ?>" target="_blank"><i class="fa fa-pinterest-p"></i> PIN IT </a>
                            </div>
                        </div>
                        <div class="mobile-show">
                            <div class="single-social--facebook text-center">
                                <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog','width=626,height=436'); return false;"><i class="fa fa-facebook"></i></a>   
                            </div>
                            <div class="single-social--twitter text-center">
                                <a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php echo $post_link; ?>&counturl=URL&via=USUARIO" target="_blank"><i class="fa fa-twitter"></i></a>
                            </div>
                            <div class="single-social--pinit text-center">
                                <a href="http://pinterest.com/pin/create/button/?url=<?php echo $post_link; ?>&media=<?php echo $imagem; ?>&description=<?php echo $titulo; ?>" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                     </div>
                  </div>
                  <div class="single-coments col-lg-12 row">
                      <div class="single-coments--title align-title--absolute">
                          <p>DEIXE UM COMENTÁRIO</p>
                      </div>
                      <div class="single-coments--container">
                          <div class="single-coments--box row">
                             <div class="right col-lg-12 col-xs-12 single-coments--box-text">
                                <div class="fb-comments" data-href="<?=$post_link?>" data-width="100%" data-numposts="5"></div>
                             </div>
                          </div>
                      </div>
                  </div>
                  <div class="single-related row col-lg-12 col-xs-12">
                      <div class="single-related--title align-title--absolute">
                          <p>VOCÊ TAMBÉM VAI GOSTAR</p>
                      </div>
                      <div class="related-block">
                          <div class="related-block--carousel">
                              <button id="button-prev--related"><i class="fa fa-angle-left"></i></button> 
                              <div id="related--carousel">
                                  <?php 
                                      $vejaTbm_query = array("post_type" => "post", "post_status" => "publish", "cat" => $categoria[0]->term_id, "post_per_page" => 9);
                                      $query_dicas = new WP_Query($vejaTbm_query);
                                      if($query_dicas->have_posts()):
                                         while($query_dicas->have_posts()):
                                            $query_dicas->the_post();
                                            $post           = get_post();
                                            $postIdDicas    = $post->ID;       
                                            $post           = get_post();        
                                            $imagem         = get_field('imagem_do_post', $postIdDicas);
                                            if($postIdDicas != $postId):
                   
                                  ?>
                                          <div class="related-container item center">
                                            <a href="<?php the_permalink(); ?>">
                                              <div class="center related-container--img">
                                                  <img src="<?php echo $imagem; ?>">
                                                  <div class="related-container--text col-xs-12 col-lg-12">
                                                      <div class="related-container--title container text-center">
                                                          <a href="<?php the_permalink(); ?>"><p><?php the_title(); ?></p></a>
                                                      </div>
                                                  </div>
                                              </div>
                                            </a>
                                          </div>
                                  <?php endif; endwhile; endif;  wp_reset_query(); ?>
                              </div>
                              <button id="button-next--related"><i class="fa fa-angle-right"></i></button>
                          </div>
                      </div>
                  </div>
             </div>
           <div class="right-block col-lg-4 right">
               <div class="container sobre-block col-mobile-6">
                   <div class="container">
                       <?php
                          $post = get_post('166'); 
                          $postId = $post->ID;
                          $imagem_sobre =  get_field('imagem_rodape');
                          $conteudo =  get_field('conteudo_reduzido');
                          $imagem_pagina =  get_field('imagem_da_pagina');

                        ?>                               
                       <div class="sobre-title">
                          <p><?php the_title(); ?></p>
                       </div>
                       <div class="sobre-img">
                           <img src="<?php echo $imagem_pagina; ?>" /> 
                       </div>
                       <div class="sobre-text">
                           <p><?php echo $conteudo; ?></p>
                           <div class="sobre-logo right">
                               <a href="<?php bloginfo('url'); ?>"><img src="<?php echo $imagem_sobre; ?>"></a>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="categoria-block col-mobile-6">
                   <div class="categoria-search">
                       <div class="categoria-icon">
                           <img src="<?php bloginfo('template_directory') ?>/images/icon-pesquisa.png" />
                       </div>
                       <form id="Pesquisa-post" action="<?php get_home_url(); ?>">
                            <input type="text" name="s" placeholder="Pesquisar"/>
                        </form>
                   </div>
                   <div class="categoria-list">
                      <h2>CATEGORIAS</h2>
                      <nav>
                          <ul>
                              <?php
                                    $categories=get_categories();
                                    foreach($categories as $category) {
                                        echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens em %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                                    }
                                ?>
                          </ul>
                      </nav>  
                  </div>
               </div>
               <div class="posts-block mobile-hide tablet-hide">
                  <div class="posts-block--title">
                      <h1>POSTS POPULARES</h1>
                  </div>
                  <?php
                      $query_popular = new WP_Query(array(
                      ‘v_orderby’ => ‘desc’, // Ordena do mais visitado para o menos visitado.
                      )
                      );

                      if($query_popular->have_posts()):while($query_popular->have_posts()):$query_popular->the_post();$postId = get_post()->ID; $imagem = get_field('imagem_do_post', $postId);
                      $categoria = get_the_category();
                      $nomeCategoria = $categoria[0]->cat_name;

                  ?>
                  <div class="row posts-container col-lg-12">
                      <div class="posts-container-align">
                          <div class="left col-lg-8 col-xs-7">
                              <div class="posts-container--category">
                                  <span><?php the_category(); ?></span>
                              </div>
                              <div class="posts-container--title">
                                  <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                              </div>
                              <div class="posts-container--date">
                                  <p><?php the_modified_time('d/m/Y'); ?></p>
                              </div>
                          </div>
                          <div class="right col-lg-4 col-xs-5 container">
                              <div class="posts-container--img">
                                  <a href="<?php the_permalink(); ?>"><img src="<?php echo $imagem ?>"></a>
                              </div>                     
                          </div>
                      </div>
                  </div>
                  <?php endwhile; endif; wp_reset_query(); ?>
               </div>
               <div class="social-right--facebook col-lg-12 col-xs-12 col-mobile-6">
                   <div class="fb-page" data-href="https://www.facebook.com/shelook.semijoias/" data-tabs="timeline" data-width="380px" data-height="298px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/shelook.semijoias/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/shelook.semijoias/">Shelook Semijoias</a></blockquote></div>
               </div>
               <div class="social-right--instagram mobile-hide tablet-hide">
                   <div class="insta-container">
                      <div class="row">
                          <div class="inta-img container col-lg-3 left">
                              <a href="https://www.instagram.com/shelook.semijoias/"><img src="<?php bloginfo('template_directory') ?>/images/instagram.jpg"></a>
                          </div>
                          <div class="insta-text container col-lg-9 left">
                              <h1>INSTAGRAM</h1>
                              <h2><a href="https://www.instagram.com/shelook.semijoias/">@shelook.semijoias</a></h2>
                          </div>
                          <!-- SnapWidget -->
                          <script src="https://snapwidget.com/js/snapwidget.js"></script>
                                    <iframe src="https://snapwidget.com/embed/code/237602" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
                      </div>
                   </div>
               </div>
          </div>
        </div>
     </div>
  </div>
</div>

<?php include 'footer.php'; ?>