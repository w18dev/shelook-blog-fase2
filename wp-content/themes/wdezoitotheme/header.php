<!DOCTYPE html>
<html>
<head>
        <?php
            $post = get_post('83'); 
            $postId = $post->ID;
            $logo = get_field('logo', $postId); 
            $titulo_seo = get_field('titulo_seo', $postId);
            $descricao_seo = removeParagraph(get_field('descricao_seo', $postId));
        ?>
        <title><?php echo $titulo_seo; ?></title>
        <meta name="description" content="<?php echo $descricao_seo ?>">
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/champs.min.css">
        <!--[if lt IE 8]>
            <script type="text/javascript" src="js/html5shiv.min.js"></script>
        <![endif]-->
</head>
<body>
      <div class="row col-lg-12 col-xs-12 center">
        <a name="topo"></a>
          <div class="header">
              <div class="container center mobile-hide tablet-hide">
                   <div class="left header--menu">
                      <div class="header-menu--top">
                          <ul>
                             <li><a href="<?php bloginfo('url'); ?>">HOME</a></li>
                             <li><a href="<?php bloginfo('url'); ?>/contato">CONTATO</a></li>
                          </ul>
                      </div>
                      <div class="header-menu--img">
                          <a href="https://www.shelook.com.br/"><img src="<?php bloginfo('template_directory') ?>/images/loja.png"></a>      
                      </div> 
                   </div>
                   <div class="right header--social">
                       <ul>
                          <li><a href="https://www.facebook.com/shelook.semijoias/"><i class="fa fa-facebook"></i></a></li>
                          <li><a href="https://www.instagram.com/shelook.semijoias"><i class="fa fa-instagram"></i></a></li>
                          <li><a href="#" id="search"><i class="fa fa-search search"></i></a></li> 
                          <div class="header-search right" id="pesquisa">
                              <form id="Pesquisa-header" action="<?php get_home_url(); ?>">
                                  <input type="text" name="s" placeholder="Pesquisar"/>
                              </form>
                          </div>  
                       </ul>
                   </div>   
              </div>
              <div class="mobile-show tablet-show">
                  <div class="left">
                      <header class="menu">
                          <div class="icon-menu" id="open-menu">
                              <s class="bar"></s>
                              <s class="bar"></s>
                              <s class="bar"></s>
                          </div>
                      </header>
                      <aside class="sidebar" id="close-menu">
                          <nav class="nav-menu myriad-regular">
                              <ul>
                                  <li><a href="<?php bloginfo('url'); ?>">HOME</a></li>
                                  <li><a href="<?php bloginfo('url'); ?>/contato">CONTATO</a></li>
                                  <li><a href="https://www.shelook.com.br/">LOJA</a></li>
                              </ul>
                          </nav>
                      </aside>
                  </div>
                  <div class="right header--social">
                       <ul>
                          <li><a href="https://www.facebook.com/shelook.semijoias/"><i class="fa fa-facebook"></i></a></li>
                          <li><a href="https://www.instagram.com/shelook.semijoias"><i class="fa fa-instagram"></i></a></li>
                          <li><a href="#" id="search-mobile"><i class="fa fa-search search"></i></a></li> 
                          <div class="header-search right" id="pesquisa-mobile">
                              <form id="Pesquisa-header--mobile" action="<?php get_home_url(); ?>">
                                  <input type="text" name="s" placeholder="Pesquisar"/>
                              </form>
                          </div>  
                       </ul>
                   </div>   
              </div>
          </div>
          <div class="row col-lg-12 col-xs-12 container center">
              <div class="header-logo text-center">
                 <a href="<?php bloginfo('url'); ?>"><img src="<?php echo $logo ?>"></a>    
              </div>    
          </div>
          <div class="row container center">
              <div class="header-menu--list text-center ">
                  <nav>
                      <ul>
                          <div class="desktop-hide notebook-hide mobile-show">
                              <button id="button-prev--menu"><i class="fa fa-angle-left"></i></button> 
                          </div>
                          <div id="carousel--category">
                              <li><a href="<?php bloginfo('url'); ?>">HOME</a></li>
                              <?php
                                  $categories=get_categories();
                                  foreach($categories as $category) {
                                      echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens em %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                                  }
                                  wp_reset_query();
                              ?>
                          </div>
                          <div class="desktop-hide notebook-hide  mobile-show">
                              <button id="button-next--menu"><i class="fa fa-angle-right"></i></button>
                          </div>
                      </ul>
                  </nav>  
    
              </div>
          </div>