<!DOCTYPE html>
<html>
<head>
		<?php
            $post = get_post('75'); 
            $postId = $post->ID;
            $titulo_seo = get_field('titulo_seo', $postId);
            $descricao_seo = removeParagraph(get_field('descricao_seo', $postId));
        ?>
        <title><?php echo $titulo_seo; ?></title>
        <meta name="description" content="<?php echo $descricao_seo ?>">
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/champs.min.css">
        <!--[if lt IE 8]>
            <script type="text/javascript" src="js/html5shiv.min.js"></script>
        <![endif]-->
</head>
<body>
     <div class="row col-lg-12 col-xs-12 center">
        <a name="topo"></a>
          <div class="header">
              <div class="container center mobile-hide tablet-hide">
                  <div class="left header--menu">
                      <div class="header-menu--top">
                          <ul>
                             <li><a href="<?php bloginfo('url'); ?>">HOME</a></li>
                             <li><a href="<?php bloginfo('url'); ?>/contato">CONTATO</a></li>
                          </ul>
                      </div>
                   </div>
                   <div class="right header--social">
                       <ul>
                          <li><a href="https://www.facebook.com/shelook.semijoias/"><i class="fa fa-facebook"></i></a></li>
                          <li><a href="https://www.instagram.com/shelook.semijoias"><i class="fa fa-instagram"></i></a></li>
                          <li><a href="#" id="search"><i class="fa fa-search search"></i></a></li>
                          <div class="header-search right" id="pesquisa">
                              <input type="text" name="txtBusca" placeholder="Pesquisar"/>
                          </div>     
                       </ul>  
                   </div>   
              </div>
              <div class="mobile-show tablet-show">
                  <div class="left">
                      <header class="menu">
                          <div class="icon-menu" id="open-menu">
                              <s class="bar"></s>
                              <s class="bar"></s>
                              <s class="bar"></s>
                          </div>
                      </header>
                      <aside class="sidebar" id="close-menu">
                          <nav class="nav-menu myriad-regular">
                              <ul>
                                  <li><a href="<?php bloginfo('url'); ?>">HOME</a></li>
                                  <li><a href="<?php bloginfo('url'); ?>/contato">CONTATO</a></li>
                                  <li><a href="https://www.shelook.com.br/">LOJA</a></li>
                              </ul>
                          </nav>
                      </aside>
                  </div>
                  <div class="right header--social">
                       <ul>
                          <li><a href="https://www.facebook.com/shelook.semijoias/"><i class="fa fa-facebook"></i></a></li>
                          <li><a href="https://www.instagram.com/shelook.semijoias"><i class="fa fa-instagram"></i></a></li>
                          <li><a href="#" id="search-mobile"><i class="fa fa-search search"></i></a></li> 
                          <div class="header-search right" id="pesquisa-mobile">
                              <form id="Pesquisa-header--mobile" action="<?php get_home_url(); ?>">
                                  <input type="text" name="s" placeholder="Pesquisar"/>
                              </form>
                          </div>  
                       </ul>
                   </div>   
              </div>
          </div>
          <div class="contato">
	          <div class="row col-lg-12 col-xs-12 container center">
	          	<!--INFORMAÇÕES DA PAGINA DE CONTATO-->
	          	  <?php
                        $post = get_post('75'); 
                        $postId = $post->ID;
                        $logo_inicial = get_field('logo_inicial');
                        $background_contato =  get_field('imagem_bkg');
                        $titulo_contato =  get_field('titulo_formulario');
                        $subtitulo_contato =  get_field('subtitulo_formulario');
                        $informacoes =  get_field('informacoes');
                        $conteudo_sobre_contato = get_field('conteudo_sobre_nos');
                        $aux = $post->post_content;

                  ?>
                  <!--//INFORMAÇÕES DA PAGINA DE CONTATO-->
	              <div class="header-logo text-center">
	                 <a href="<?php bloginfo('url'); ?>"><img src="<?php echo $logo_inicial ?>"></a>    
	              </div>    
	          </div>
			  <div class="container center col-lg-12 col-xs-12">
				 <div class="contato-mobile container">
				 	<!--INFORMAÇÕES DA PAGINA SOBRE NÓS--> 
				 	<?php
                        $post = get_post('166'); 
                        $postId = $post->ID;
                        $imagem_sobre =  get_field('imagem_rodape');
                        $subt_sobre =  get_field('subtitulo');
                        $conteudo =  get_field('conteudo');

                      ?> 
                     <!--//INFORMAÇÕES DA PAGINA SOBRE NÓS--> 
	      			<div class="contato-img">
                    	<img src="<?php echo $background_contato ?>" />
                	</div> 
	                <div class="contato-block item row">
	                    <div class="contato-container">
		                    <div class="block-details--left container col-lg-6 col-xs-12 left">
		                    	<div class="contato-mobile container">
		                    		<div class="contato-mobile container">
		                    			<div class="contato-mobile container">
					                        <div class="block-details-title">
					                        	<h1><?php the_title(); ?></h1>
					                        	<div class="oswald-bold">
					                        		<h2><?php echo $subt_sobre; ?></h2>
					                        	</div>
					                        	<div class="block-details-content">
					                        		<p><?php echo $aux; ?></p>
					                        	</div>
					                        	<div class="block-details-img row right">
					                        		<a href="<?php bloginfo('url'); ?>"><img src="<?php echo $imagem_sobre ?>"></a> 
					                        	</div> 
					                        	<div class="block-details-social row">
					                        	 <?php
						                                $post = get_post('75'); 
						                                while( have_rows('redes_sociais') ): the_row(); 
						                                $postId = $post->ID;
						                                $icone =  get_sub_field('icone_social');
						                                $link =  get_sub_field('link_social');
					                              ?>
					                              	<div class="left">
						                        		<ul>
							                        		<li><a href="<?php echo $link ?>"><i class="<?php echo $icone?>"></i></a></li>
						                        		</ul>
					                              	</div>
					                        		<?php endwhile; wp_reset_query(); ?>
					                        	</div>
					                        </div>
		                    			</div>
		                    		</div>
		                    	</div>
		                    </div>
		                    <div class="block-details--right container col-lg-6 col-xs-12 right">
		                    	<div class="contato-mobile container">
		                    		<div class="contato-mobile container">
				                        <div class="block-details-title">
				                        	<h1>Fale conosco</h1>
				                        	<h2><?php echo $subtitulo_contato ?></h2>
				                        </div>
				                        <div class="block-details-content">
				                        	<?php echo $informacoes ?>
				                        </div>
				                        <div class="block-details-contact">
				                        	<form>
				                        		<div class="form-field">
				                        			<input type="text" name="txtNome" placeholder="Nome Completo" />
				                        		</div>
				                        		<div class="form-field">
				                        			<input type="text" name="txtNome" placeholder="Assunto" />					 
				                        		</div>
				                        		<div class="form-field">
					                        		<textarea placeholder="Mensagem"></textarea>
				                        		</div>
				                        		<div class="button-field">
				                        			<button>ENVIAR</button>
				                        		</div>
				                        	</form>
				                        </div>
		                    		</div> 
			                    </div>
		                    </div>
		                </div>
	                </div>
				 </div>
			 </div>
          </div>
    </div>
	<footer class="footer footer-contato text-center">
		<div class="footer-title">
			<p>NOS SIGA NO INSTAGRAM</p>
		</div>
		<div class="footer-slider--instagram">
			<!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/2a13c45abfa052ac83f57ec2c75ea8a6.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
		</div>
		<div class="container center">
			<div class="footer-icon left">
				<a href="#"><img src="<?php bloginfo('template_directory') ?>/images/logo-footer.png"></a>
			</div>
			<div class="footer-copiryght right col-xs-12">
				<div class="lato-regular">
					<p>© 2016 SHE LOOK SEMI JOIAS. Todos os direitos reservados</p>
				</div>
				<div class="triangulo pracima oswald-bold mobile-hide tablet-hide">
	                <a href="#topo">TOPO</a>
	            </div>
			</div>	
		</div>
			
	</footer>

	 <script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.3.min.js"></script>
	 <script src="<?php bloginfo('template_directory'); ?>/js/champs.min.js"></script>
	 <script>(function(d, s, id) {
	      var js, fjs = d.getElementsByTagName(s)[0];
	      if (d.getElementById(id)) return;
	      js = d.createElement(s); js.id = id;
	      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8";
	      fjs.parentNode.insertBefore(js, fjs);
	    }(document, 'script', 'facebook-jssdk'));
	 </script>
</body>
</html>
