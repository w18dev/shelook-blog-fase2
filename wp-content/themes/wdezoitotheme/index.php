<?php include 'header.php'; ?>

          <div class="row container center col-lg-12 col-xs-12 index--align">
              <div class="row">
                  <div class="carousel-destaque col-lg-7 col-xs-12 left">
                      <div id="banner--carousel">
                              <?php
                                $post = get_post('83'); 
                                while( have_rows('carousel') ): the_row(); 
                                $postId = $post->ID;
                                $imagem =  get_sub_field('imagem');
                                $titulo =  get_sub_field('titulo');
                                $conteudo =  get_sub_field('conteudo');
                                $link_banner =  get_sub_field('link_banner');
                              ?>
                                  <div class="banner-block item">
                                    <a href="<?php echo $link_banner; ?>">
                                      <img src="<?php echo $imagem; ?>" />
                                      <div class="block-details--banner text-center center">
                                          <div class="block-details--content">
                                              <div class="block-details--content-title">
                                                  <h1><?php echo $titulo ?></h1>   
                                              </div>
                                              <div class="col-xs-8 center">
                                                  <h2><?php echo $conteudo ?></h2>  
                                              </div>
                                          </div>
                                      </div>
                                    </a>
                                  </div>
                              <?php endwhile; ?>
                      </div>    
                  </div>
                  <div class="carousel-loja col-lg-4 col-xs-12 right">
                      <div class="loja-container">
                          <div class="loja-title align-title--absolute">
                              <p>LOJA</p>
                          </div>
                          <div class="loja-block">
                              <button id="button-prev"><i class="fa fa-angle-left"></i></button>
                              <div id="loja--carousel">
                                 <?php
                                    $post = get_post('83'); 
                                    while( have_rows('carousel_loja') ): 
                                      the_row(); 
                                    $postId = $post->ID;
                                    $imagem =  get_sub_field('imagem');
                                    $nome =  get_sub_field('nome');
                                    $descricao =  get_sub_field('descricao');
                                    $link = get_sub_field('link_produto');
                                  ?>
                                      <div class="item container center">
                                          <div class="center loja-container--img">
                                            <a href="<?php echo $link; ?>">
                                              <img src="<?php echo $imagem ?>">
                                              <div class="block-details--loja text-center">
                                                  <h1><?php echo $nome ?></h1>
                                                  <h2><?php echo $descricao ?></h2>
                                              </div>  
                                            </a>
                                          </div>
                                      </div>
                                  <?php endwhile; ?>
                              </div>  
                              <button id="button-next"><i class="fa fa-angle-right"></i></button> 
                          </div>    
                      </div>  
                  </div>        
              </div>
              <div class="row slider">
                  <div class="text-center slider-title align-title--absolute">
                       <p>TOP POSTS</p> 
                  </div>
                  <div class="slider-block container col-lg-12 col-xs-12 center">
                      <button id="button-prev--slider"><i class="fa fa-angle-left"></i></button> 
                      <div id="slider--carousel">
                          <?php
                              $query_popular = new WP_Query(array(
                                'v_orderby'   => 'desc', // Ordena do mais visitado para o menos visitado.
                              )
                              );

                              if($query_popular->have_posts()):while($query_popular->have_posts()):$query_popular->the_post();$postId = get_post()->ID; $imagem = get_field('imagem_do_post', $postId);
                              $categoria = get_the_category();
                              $nomeCategoria = $categoria[0]->cat_name;

                          ?>
                                  
                                  <div class="slider-container item center">
                                      <a href="<?php the_permalink(); ?>">
                                        <div class="center slider-container--img">
                                              <img src="<?php echo $imagem; ?>" />
                                              <div class="slider-content">
                                                  <div class="container slider-content--align col-xs-12">
                                                       <div class="slider-content--title text-center col-xs-10 col-lg-12 center">
                                                          <p><?php the_title(); ?></p>
                                                       </div>
                                                       <div class="slider-content--category text-center vollkorn-regular">
                                                          <span><?php echo $nomeCategoria; ?></span>
                                                       </div>
                                                  </div>
                                              </div>
                                        </div>
                                      </a>
                                  </div>
                          <?php endwhile; endif; wp_reset_query(); ?>
                          
                       </div>
                       <button id="button-next--slider"><i class="fa fa-angle-right"></i></button>        
                 </div>
               </div>
               <div class="col-lg-12 col-xs-12 row">
                   <div class="posts-block col-lg-8 col-xs-12 left">
                     <?php 
                             $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                             $query_arg = array("post_type" => "post", "post_status" => "publish", "paged" => $paged);
                             $queryPosts = new WP_Query($query_arg);
                             $category_arr = null;
                             if($queryPosts->have_posts()):
                               while($queryPosts->have_posts()): 
                                  $aux = 0;
                                  $queryPosts->the_post();
                                  $post = get_post();        
                                  $postId = $post->ID;       
                                  //$imagem = get_post_meta($postId, 'wpcf-imagem-do-post', true);
                                  $imagem           = get_field('imagem_do_post', $postId);
                                  $conteudo         = removeParagraph(get_field('conteudo_do_post', $postId));
                                  $post_link        = get_permalink($post->ID);
                                  $titulo           = get_the_title($post->ID);
                                  $categoria        = get_the_category();
                                  $category_arr[]   = $categoria;

                                //if (function_exists('wp_corenavi')) {wp_reset_query();}                  
                       ?>
                          <div class="row posts-container"> 
                            <a name="POSTS"></a>
                            <div class="posts-img col-lg-5 col-xs-12">
                                <a href="<?php the_permalink(); ?>"><img src="<?php echo $imagem; ?>" /></a>   
                            </div>
                            <div class="posts-content container col-lg-6 col-xs-12">
                                <div class="posts-category">
                                    <span><?php the_category(); ?></span>
                                </div>
                                <div class="posts-title">
                                   <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                                </div>
                                <div class="posts-author">
                                    <span><?php the_modified_time('d/m/Y'); ?> por <i><?php the_author_posts_link(); ?></i></span>
                                </div>
                                <div class="posts-about">
                                    <p><?php echo $conteudo ?></p>
                                </div>
                                <div class="posts-ver">
                                    <a href="<?php the_permalink(); ?>">VER O POST</a>
                                </div>
                                <div class="posts-social">
                                    <div class="mobile-hide">
                                        <div class="posts-social--facebook text-center">
                                            <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog','width=626,height=436'); return false;"><i class="fa fa-facebook"></i> COMPARTILHE </a>   
                                        </div>
                                        <div class="posts-social--twitter text-center">
                                            <a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php echo $post_link; ?>&counturl=URL&via=USUARIO" target="_blank"><i class="fa fa-twitter"></i> TWEET </a>
                                        </div>
                                        <div class="posts-social--pinit text-center">
                                            <a href="http://pinterest.com/pin/create/button/?url=<?php echo $post_link; ?>&media=<?php echo $imagem; ?>&description=<?php echo $titulo; ?>" target="_blank"><i class="fa fa-pinterest-p"></i> PIN IT </a>
                                        </div>
                                    </div>
                                    <div class="mobile-show">
                                         <div class="posts-social--facebook text-center">
                                            <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog','width=626,height=436'); return false;"><i class="fa fa-facebook"></i></a>   
                                        </div>
                                        <div class="posts-social--twitter text-center">
                                            <a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php echo $post_link; ?>&counturl=URL&via=USUARIO" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </div>
                                        <div class="posts-social--pinit text-center">
                                            <a href="http://pinterest.com/pin/create/button/?url=<?php echo $post_link; ?>&media=<?php echo $imagem; ?>&description=<?php echo $titulo; ?>" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                        <div class="dicas-block col-lg-11 col-xs-12 row">
                            <div class="text-center dicas-title align-title--absolute">
                                <p>DICAS DO POST</p> 
                            </div>
                            <div class="dicas-block--carousel">
                                <div class="dicas-block--align">
                                  <button class="button-prev--dicas"><i class="fa fa-angle-left"></i></button> 
                                    <div class="dicas--carousel">
                                    <?php
                                          $query_argCarousel  = array("post_type" => "produtos", "cat" => $categoria[0]->cat_ID);
                                          $query_carousel     = new WP_Query($query_argCarousel);
                                          if($query_carousel->have_posts()):
                                            $query_carousel->the_post();
                                            while( have_rows ('produtos_relacionados') ):
                                            the_row();
                                            $post = get_post();        
                                            $postId_carousel = $post->ID;
                                            $imagem = get_sub_field('imagem_do_produto', $postId_carousel);  
                                            $nome = get_sub_field('nome_do_produto', $postId_carousel);  
                                            $valor = get_sub_field('valor_do_produto', $postId_carousel);  
                                            $link = get_sub_field('link_do_produto', $postId_carousel);  
                                    ?>
                                        <div class="dicas-container item center">
                                            <div class="center dicas-container--img">
                                                <img src="<?php echo $imagem; ?>">
                                                <a href="<?php echo $link; ?>">
                                                    <div class="dicas-container--block">
                                                        <div class="dicas-container--text text-center">
                                                            <p>COMPRAR</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="text-center">
                                                <h1><?php echo $nome ?></h1>
                                                <h2><?php echo $valor ?></h2>
                                                <a href="<?php echo $link; ?>">COMPRAR</a>
                                            </div>
                                        </div>
                                    <?php endwhile; endif; #$wp_reset_query();?>
                                    </div>
                                  <button class="button-next--dicas"><i class="fa fa-angle-right"></i></button> 
                                </div>
                            </div>
                        </div>
                        <?php 
                            endwhile; 
                          endif;
                          wp_reset_query();
                        ?>
                        
                        <div class="row container text-center pagenavi col-lg-11">
                            <div id="pagination">
                                <div class="nav-pag">
                                    <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
                                </div>
                                <div class="navigation">
                                    <div class="nav-previous"><?php previous_posts_link( __( '< ', 'shelook-blog-fase2' ) ); ?></div>
                                    <div class="nav-next"><?php next_posts_link ( __( ' >', 'shelook-blog-fase2' ) ); ?></div>
                                </div>  
                            </div>
                        </div>
                   </div>
                   <div class="right-block col-lg-4 col-xs-12 right">
                      <div class="container sobre-block col-mobile-6">
                          <div class="container">
                              <!--INFORMAÇÕES DA PAGINA SOBRE NÓS--> 
                              <?php
                                $post = get_post('166'); 
                                $postId = $post->ID;
                                $imagem_sobre =  get_field('imagem_rodape');
                                $subt_sobre =  get_field('subtitulo');
                                $conteudo =  get_field('conteudo_reduzido');
                                $imagem_pagina =  get_field('imagem_da_pagina');

                              ?> 
                              <!--//INFORMAÇÕES DA PAGINA SOBRE NÓS-->                                 
                              <div class="sobre-title">
                                 <p>SOBRE NÓS</p>
                              </div>
                              <div class="sobre-img">
                                  <img src="<?php echo $imagem_pagina ?>" /> 
                              </div>
                              <div class="sobre-text">
                                  <?php echo $conteudo ?>
                                  <div class="sobre-logo right">
                                      <a href="<?php bloginfo('url'); ?>"><img src="<?php echo $imagem_sobre ?>"></a>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="categoria-block col-mobile-6">
                          <div class="categoria-search">
                              <div class="categoria-icon">
                                  <img src="<?php bloginfo('template_directory') ?>/images/icon-pesquisa.png" />
                              </div>
                              <form id="Pesquisa-index" action="<?php get_home_url(); ?>">
                                  <input type="text" name="s" placeholder="Pesquisar"/>
                              </form>
                          </div>
                          <div class="categoria-list">
                              <h2>CATEGORIAS</h2>
                              <nav>
                                  <ul>
                                      <?php
                                            $categories=get_categories();


                                            foreach($categories as $category) {
                                                echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens em %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                                            }
                                        ?>
                                  </ul>
                              </nav>  
                          </div>
                      </div>
                      <div class="social-right--facebook col-lg-12 col-xs-12  col-mobile-6">
                          <div class="fb-page" data-href="https://www.facebook.com/shelook.semijoias/" data-tabs="timeline" data-width="380px" data-height="298px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/shelook.semijoias/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/shelook.semijoias/">Shelook Semijoias</a></blockquote></div>
                      </div>
                      <div class="social-right--instagram mobile-hide tablet-hide">
                          <!-- SnapWidget -->
                          <div class="insta-container">
                              <div class="row">
                                  <div class="inta-img container col-lg-3 left">
                                      <a href="https://www.instagram.com/shelook.semijoias/"><img src="<?php bloginfo('template_directory') ?>/images/instagram.jpg"></a>
                                  </div>
                                  <div class="insta-text container col-lg-9 left">
                                      <h1>INSTAGRAM</h1>
                                      <h2><a href="https://www.instagram.com/shelook.semijoias/">@shelook.semijoias</a></h2>
                                  </div>

                                  <!-- SnapWidget -->
                                  <script src="https://snapwidget.com/js/snapwidget.js"></script>
                                  <iframe src="https://snapwidget.com/embed/code/237602" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
                              </div>
                          </div>
                      </div>
                   </div>
               </div>
          </div>

<?php include 'footer.php'; ?>


            