<?php include 'header.php'; ?>
          <div class="row container center col-lg-12 col-xs-12 index--align">
                <div class="row col-lg-12 col-xs-12">
                  <?php 
                      $category_banner = (get_query_var('cat'));
                      $query_arg  = array("post_type" => "categoria_banner", "cat" => $category_banner);
                      $query = new WP_Query($query_arg);
                      //showResult($query);

                      if($query->have_posts()):
                        $query->the_post();
                        $post = get_post();        
                        $postId = $post->ID;
                        $imagem_banner = get_field('imagem_da_categoria', $postId);
                        //Para pegar o nome da categoria 
                        $nomeCategoria = get_cat_name($category_banner);                        
                  ?>
                      <div class="banner-single">
                          <div class="banner-single--block">
                              <div class="banner-img" style="background-image: url(<?php echo $imagem_banner ?>)">
                                <div class="container center">
                                    <div class="banner-text text-center">
                                        <span><?php  echo $nomeCategoria; ?></span>
                                    </div>                            
                                </div>
                              </div>
                          </div>
                      </div>
                  <?php 
                      else:
                  ?>
                    :)
                  <?php
                      endif;
                      wp_reset_query();
                  ?>
               </div>
               <div class="col-lg-12 col-xs-12 row">
                   <div class="posts-block col-lg-8 col-xs-12 left">
                       <?php 
                               $category = (get_query_var('cat'));
                               $query = "status=publish&posts_per_page=9&cat=".$category;
                              if(query_posts($query)):
                                 while(have_posts()):
                                    the_post();
                                    $postId = $post->ID;       
                                    $post = get_post();        
                                    $imagem = get_field('imagem_do_post', $postId);
                                    $conteudo = removeParagraph(get_field('conteudo_do_post', $postId));
           
                         ?>
                        <div class="row posts-container"> 
                            <a name="POSTS"></a>
                            <div class="posts-img col-lg-5">
                                <a href="<?php the_permalink(); ?>"><img src="<?php echo $imagem; ?>" /></a>   
                            </div>
                            <div class="posts-content container col-lg-6 col-xs-12">
                                <div class="posts-category">
                                    <span><?php the_category(); ?></span>
                                </div>
                                <div class="posts-title">
                                   <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                                </div>
                                <div class="posts-author">
                                    <span><?php the_modified_time('d/m/Y'); ?> por <i><?php the_author(); ?></i></span>
                                </div>
                                <div class="posts-about">
                                    <p><?php echo $conteudo ?></p>
                                </div>
                                <div class="posts-ver">
                                    <a href="<?php the_permalink(); ?>">VER O POST</a>
                                </div>
                                <div class="posts-social">
                                    <div class="mobile-hide">
                                        <div class="posts-social--facebook text-center">
                                            <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog','width=626,height=436'); return false;"><i class="fa fa-facebook"></i> COMPARTILHE </a>     
                                        </div>
                                        <div class="posts-social--twitter text-center">
                                            <a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php echo $post_link; ?>&counturl=URL&via=USUARIO" target="_blank"><i class="fa fa-twitter"></i> TWEET </a>
                                        </div>
                                        <div class="posts-social--pinit text-center">
                                            <a href="http://pinterest.com/pin/create/button/?url=<?php echo $post_link; ?>&media=<?php echo $imagem; ?>&description=<?php echo $titulo; ?>" target="_blank"><i class="fa fa-pinterest-p"></i> PIN IT </a>
                                        </div>
                                    </div>
                                    <div class="mobile-show">
                                        <div class="posts-social--facebook text-center">
                                            <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog','width=626,height=436'); return false;"><i class="fa fa-facebook"></i></a>   
                                        </div>
                                        <div class="posts-social--twitter text-center">
                                            <a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php echo $post_link; ?>&counturl=URL&via=USUARIO" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </div>
                                        <div class="posts-social--pinit text-center">
                                            <a href="http://pinterest.com/pin/create/button/?url=<?php echo $post_link; ?>&media=<?php echo $imagem; ?>&description=<?php echo $titulo; ?>" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; endif;
                            wp_reset_query();
                        ?>
                        <div class="row container text-center pagenavi col-lg-11">
                            <div id="pagination">
                                <div class="nav-pag">
                                    <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
                                </div>
                                <div class="navigation">
                                    <div class="nav-previous"><?php previous_posts_link( __( '< ', 'shelook-blog-fase2' ) ); ?></div>
                                    <div class="nav-next"><?php next_posts_link ( __( ' >', 'shelook-blog-fase2' ) ); ?></div>
                                </div>  
                            </div>
                        </div>
                   </div>
                   <div class="right-block col-lg-4 right">
                      <div class="container sobre-block col-mobile-6">
                          <div class="container">
                              <!--INFORMAÇÕES DA PAGINA SOBRE NÓS--> 
                              <?php
                                $post = get_post('166'); 
                                $postId = $post->ID;
                                $imagem_sobre =  get_field('imagem_rodape');
                                $subt_sobre =  get_field('subtitulo');
                                $conteudo =  get_field('conteudo_reduzido');
                                $imagem_pagina =  get_field('imagem_da_pagina');

                              ?> 
                              <!--//INFORMAÇÕES DA PAGINA SOBRE NÓS-->                                 
                              <div class="sobre-title">
                                 <p>SOBRE NÓS</p>
                              </div>
                              <div class="sobre-img">
                                  <img src="<?php echo $imagem_pagina ?>" /> 
                              </div>
                              <div class="sobre-text">
                                  <?php echo $conteudo ?>
                                  <div class="sobre-logo right">
                                      <a href="#"><img src="<?php echo $imagem_sobre ?>"></a>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="categoria-block col-mobile-6">
                          <div class="categoria-search">
                              <div class="categoria-icon">
                                  <img src="<?php bloginfo('template_directory') ?>/images/icon-pesquisa.png" />
                              </div>
                              <form id="Pesquisa-category" action="<?php get_home_url(); ?>">
                                  <input type="text" name="s" placeholder="Pesquisar"/>
                              </form>
                          </div>
                          <div class="categoria-list">
                              <h2>CATEGORIAS</h2>
                              <nav>
                                  <ul>
                                      <?php
                                            $categories=get_categories();


                                            foreach($categories as $category) {
                                                echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens em %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                                            }
                                        ?>
                                  </ul>
                              </nav>  
                          </div>
                      </div>
                      <div class="social-right--facebook col-lg-12 col-mobile-6 col-sm-6 col-xs-12">
                          <div class="fb-page" data-href="https://www.facebook.com/shelook.semijoias/" data-tabs="timeline" data-width="380px" data-height="298px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/shelook.semijoias/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/shelook.semijoias/">Shelook Semijoias</a></blockquote></div>
                      </div>
                      <div class="social-right--instagram mobile-hide tablet-hide">
                          <!-- SnapWidget -->
                          <div class="insta-container">
                              <div class="row">
                                  <div class="inta-img container col-lg-3 left">
                                      <a href="https://www.instagram.com/shelook.semijoias/"><img src="<?php bloginfo('template_directory') ?>/images/instagram.jpg"></a>
                                  </div>
                                  <div class="insta-text container col-lg-9 left">
                                      <h1>INSTAGRAM</h1>
                                      <h2><a href="https://www.instagram.com/shelook.semijoias/">@shelook.semijoias</a></h2>
                                  </div>
                                  <!-- SnapWidget -->
                                  <!-- SnapWidget -->
                                  <script src="https://snapwidget.com/js/snapwidget.js"></script>
                                  <iframe src="https://snapwidget.com/embed/code/237602" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
                              </div>
                          </div>
                      </div>
                   </div>
               </div>
          </div>

<?php include 'footer.php'; ?>
<script type="text/javascript">
  $(window).load(function(){
    var category  = <?=$aux?>,
        url       = <?php bloginfo('url'); ?>
    selecionarItemMenu(url, category);
    $("a[href='http:uahsaushuahsuas/asuhasuhas/category/nome']").addClass("active");
  });
  function selecionarItmeMenu(url, category){
    $("a[href="+url+"/category/"+category+"']").addClass("active");
  }
</script>

            